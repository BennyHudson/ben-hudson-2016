<?php
	 
	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	function tastic_theme_setup() {
		include_once( get_stylesheet_directory() . '/acf/acf.php' );
		add_filter('acf/settings/path', 'my_acf_settings_path');
		add_filter('acf/settings/dir', 'my_acf_settings_dir');
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'tastic_posts');
		add_action('init', 'tastic_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'tastic_scripts' );
		add_action( 'admin_init', 'tastic_users' );
		add_filter('admin_footer_text', 'tastic_footer');
		add_action('wp_dashboard_setup', 'tastic_dashboard');
		add_action('admin_head', 'tastic_admin');
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_image_size( 'gallery-thumb', 400, 400, true );
		add_image_size( 'feature-square', 500, 450, true );
		add_image_size( 'letterbox', 1200, 675, true );
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'users');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'acf');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'welcome');
	get_template_part('functions/include', 'adminstyle');
	//get_template_part('functions/include', 'roles');

	$result = add_role( 'site_owner', __(
	'Site Owner' ),
	array( 

		// Dashboard
		'read' => true, // Access to Dashboard and Users -> Your Profile.
		'update_core' => false, // Can NOT update core. I added a plugin for this.
		'edit_posts' => true, //Access to Posts, Add New, Comments and moderating comments.

		// Posts
		'edit_posts' => true, //Access to Posts, Add New, Comments and moderating comments.
		'create_posts' => true, // Allows user to create new posts
		'delete_posts' => true, // Can delete posts.
		'publish_posts' => true, // Can publish posts. Otherwise they stay in draft mode.
		'delete_published_posts' => true, // Can delete published posts.
		'edit_published_posts' => true, // Can edit posts.
		'edit_others_posts' => true, // Can edit other users posts.
		'delete_others_posts' => true, // Can delete other users posts.

		// Categories, comments and users
		'manage_categories' => true, // Access to managing categories.
		'moderate_comments' => true, // Access to moderating comments. Edit posts also needs to be set to true.
		'edit_comments' => true, // Comments are blocked out for this user.
		'edit_users' => true, // Can not view other users.
		'create_users' => true,
		'list_users' => true,
		'promote_users' => true,
		'remove_users' => true,

		// Pages
		'edit_pages' => true, // Access to Pages and Add New (page).
		'publish_pages' => true, // Can publish pages.
		'edit_other_pages' => true, // Can edit other users pages.
		'edit_published_ pages' => true, // Can edit published pages.
		'delete_pages' => true, // Can delete pages.
		'delete_others_pages' => true, // Can delete other users pages.
		'delete_published_pages' => true, // Can delete published pages.

		// Media Library
		'upload_files' => true, // Access to Media Library.

		// Appearance
		'edit_themes_options' => false, // Access to Appearance panel options.
		'switch_themes' => false, // Can not switch themes.
		'delete_themes' => false, // Can NOT delete themes.
		'install_themes' => false, // Can not install a new theme.
		'update_themes' => false, // Can NOT update themes.
		'edit_themes' => false, // Can not edit themes - through the appearance editor.

		// Plugins
		'activate_plugins' => false, // Access to plugins screen.
		'edit_plugins' => false, // Can not edit plugins - through the appearance editor.
		'install_plugins' => false, // Access to installing a new plugin.
		'update_plugins' => false, // Can update plugins.
		'delete_plugins' => false, // Can NOT delete plugins.

		// Settings
		'manage_options' => false, // Can not access Settings section.

		// Tools
		'import' => false, // Can not access Tools section.

	) );

	function custom_excerpt_length( $length ) {
		return 100;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

?>
