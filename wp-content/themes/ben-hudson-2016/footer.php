			<footer>
				<?php if(!is_singular('works')) { ?>
					<section class="footer-cta">
						<section class="container main">
							<a href="<?php bloginfo('url'); ?>/contact">Let's Start Something</a>
						</section>
					</section>
				<?php } ?>
				<div class="footer-main">
					<section class="container">
						<aside>
							<p>&copy; Ben Hudson <?php the_time('Y'); ?></p>
						</aside>
						<aside>
							<ul class="socials">
								<li><a href="https://twitter.com/BennyHudson" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
								<li><a href="https://www.instagram.com/bennyhudson/" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<li><a href="https://github.com/BennyHudson" target="_blank"><i class="fa fa-github-square"></i></a></li>
								<li><a href="https://www.linkedin.com/in/ben-hudson-8a978529" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
							</ul>
						</aside>
					</section>
				</div>
			</footer>
		</section>
	</section>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">

	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
