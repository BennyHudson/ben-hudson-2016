<?php get_header(); ?>

	<style type="text/css">
		#menu-item-132 a {color: #7d7d7d;}
		#menu-item-132:hover a {color: #2980b9;}
	</style>

	<?php
		$morestring = '<!--more-->';
		$explode_content = explode( $morestring, $post->post_content );
		$content_before = apply_filters( 'the_content', $explode_content[0] );
		$content_after = apply_filters( 'the_content', $explode_content[1] );

		$url = get_field('url');
		$url = preg_replace('#^https?://#', '', rtrim($url,'/'));
	?>

	<section class="work-feature">
		<section class="container">
			<h1 class="work-title"><?php the_title(); ?></h1>
			<section class="browser">
				<section class="browser-bar">
					<ul>
						<li></li>
						<li></li>
						<li></li>
					</ul>
					<div class="address-bar">
						<p><?php the_title(); ?></p>
					</div>
				</section>
				<?php the_post_thumbnail('full'); ?>
			</section>
		</section>
	</section>

	<section class="work-main">
		<?php if(get_field('huge_screens')) { ?>
			<section class="container main">
				<article>
					<div class="work-meta">
						<span><i class="fa fa-calendar"></i> <?php the_field('launch_date'); ?></span>
						<?php if(get_field('url')) { ?>
							<span><i class="fa fa-globe"></i> <a href="<?php the_field('url'); ?>" target="_blank"><?php echo $url; ?></a></span>
						<?php } ?>
						<span><i class="fa fa-bullseye"></i> <?php the_field('company'); ?></span>
					</div>
					<?php echo $content_before; ?>
				</article>
			</section>
			<?php while(the_repeater_field('huge_screens')) { ?>
				<?php
					$image_id = get_sub_field('screenshot');

					$thumbnail = wp_get_attachment_image_src($image_id, 'thumbnail');
					$thumbnail_url = $thumbnail[0];

					$full = wp_get_attachment_image_src($image_id, 'full');
					$full_url = $full[0];

				?>
				<section class="huge-screen">
					<section class="container main">
						<section class="browser">
							<section class="browser-bar">
								<ul>
									<li></li>
									<li></li>
									<li></li>
								</ul>
							</section>
							<img src="<?php echo $full_url; ?>" alt="<?php the_title(); ?>">
						</section>	
					</section>	
				</section>
			<?php } ?>
			<section class="container main">
				<article>
					<?php echo $content_after; ?>
				</article>
			</section>
		<?php } else { ?>
			<section class="container main">
				<article>
					<div class="work-meta">
						<span><i class="fa fa-calendar"></i> <?php the_field('launch_date'); ?></span>
						<?php if(get_field('url')) { ?>
							<span><i class="fa fa-globe"></i> <a href="<?php the_field('url'); ?>" target="_blank"><?php echo $url; ?></a></span>
						<?php } ?>
						<span><i class="fa fa-bullseye"></i> <?php the_field('company'); ?></span>
					</div>
					<?php the_content(); ?>
				</article>
			</section>
		<?php } ?>
	</section>

	<section class="footer-cta">
		<section class="container main">
			<a href="<?php bloginfo('url'); ?>/contact">Let's Start Something</a>
		</section>
	</section>

	<section class="work-more">
		<section class="container main">
			<h2 class="section-title">More Work</h2>
			<?php 
				$currentID = get_the_ID();
			    $args = array(
			        'post_type'             => 'works',
			        'posts_per_page'        => 3,
			        'orderby'               => 'rand',
			        'exclude'				=> $currentID
			    );  
			    $the_query = new WP_Query( $args );
			?>
			<?php if($the_query->have_posts() ) { ?>
				<ul class="work-list grid">
					<?php while($the_query->have_posts()) { ?>
						<?php $the_query->the_post(); ?>
							<?php get_template_part('includes/partial', 'work'); ?>    
				    	<?php wp_reset_postdata(); ?>
					<?php } ?>
				</ul>
			<?php } ?>
		</section>
	</section>

<?php get_footer(); ?>
