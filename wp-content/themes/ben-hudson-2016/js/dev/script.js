$(document).ready(function() {

	jQuery('.container').fitVids();
    jQuery('.parallax-window').parallax();

	var footerHeight = $('footer').outerHeight();
    var headerHeight = $('header').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);
    $('.footer-fix').css('padding-top', headerHeight);

    $('.mobile-nav-trigger').click(function(e) {
        e.preventDefault();
        $('.mobile-nav').slideToggle();
        $(this).toggleClass('active');
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
