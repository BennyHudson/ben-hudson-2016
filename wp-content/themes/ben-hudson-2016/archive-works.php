<?php get_header(); ?>

	<style type="text/css">
		#menu-item-132 a {color: #7d7d7d;}
		#menu-item-132:hover a {color: #2980b9;}
	</style>

	<?php 
		global $query_string;
		query_posts( $query_string . '&posts_per_page=-1&orderby=rand' );
		if (have_posts()) : 
	?>
		<section class="container main">
			<h1 class="work-title alt">Works</h1>
			<ul class="work-list grid">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('includes/partial', 'work'); ?>
				<?php endwhile; ?>
			</ul>
		</section>
	<?php endif; ?>

<?php get_footer(); ?>
