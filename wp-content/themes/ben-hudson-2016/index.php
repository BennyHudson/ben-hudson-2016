<?php get_header(); ?>

	<section class="container main">
		<?php 
			$postslist = get_posts('numberposts=1');
		    foreach ($postslist as $post) {
		?>
			<?php setup_postdata($post); ?>
				<div class="feature-post">
					<aside class="feature">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('feature-square'); ?></a>
					</aside>
					<aside class="excerpt">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<div class="excerpt-clip">
							<?php the_excerpt(); ?>
						</div>
						<div class="post-meta">
							<span><i class="fa fa-calendar"></i> <?php the_time('jS F Y'); ?></span>
							<span><i class="fa fa-folder-open"></i> <?php the_category( ', ' ); ?></span>
							<span><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="fa fa-arrow-circle-right"></i> Read More</a></span>
						</div>
					</aside>
				</div>
			<?php wp_reset_postdata(); ?>	
		<?php } ?>
		<?php 
			global $query_string;
			query_posts( $query_string . '&posts_per_page=10000&offset=1' );
			if (have_posts()) : 
		?>
			<ul class="post-list">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('includes/partial', 'excerpt'); ?>	
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</section>

<?php get_footer(); ?>
