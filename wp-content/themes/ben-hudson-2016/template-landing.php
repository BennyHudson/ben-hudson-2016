<?php
	//Template Name: Landing Page
	get_header();
?>

	<section class="home-feature parallax-window" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/home-feature.jpg" data-speed="0.5">
		<section class="container">
			<h2>Hand Crafted<br>Web Development</h2>
		</section>
	</section>

	<?php
		$morestring = '<!--more-->';
		$explode_content = explode( $morestring, $post->post_content );
		$content_before = apply_filters( 'the_content', $explode_content[0] );
		$content_after = apply_filters( 'the_content', $explode_content[1] );
	?>

	<section class="home-content">
		<section class="container ultra narrow">
			<?php echo $content_before; ?>
		</section>
	</section>

	<section class="work-more">
		<section class="container ultra">
			<?php 
			    $args = array(
			        'post_type'             => 'works',
			        'posts_per_page'        => 8,
			        'orderby'               => 'rand'
			    );  
			    $the_query = new WP_Query( $args );
			?>
			<?php if($the_query->have_posts() ) { ?>
				<ul class="work-list grid">
					<?php while($the_query->have_posts()) { ?>
						<?php $the_query->the_post(); ?>
							<?php get_template_part('includes/partial', 'work'); ?>    
				    	<?php wp_reset_postdata(); ?>
					<?php } ?>
				</ul>
			<?php } ?>
		</section>
	</section>

	<section class="home-content">
		<section class="container ultra narrow">
			<?php echo $content_after; ?>
		</section>
	</section>

<?php get_footer(); ?>
