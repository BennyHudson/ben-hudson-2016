<li class="excerpt">
	<h4 class="fixed-title"><i class="fa fa-folder-open"></i> <?php the_category( ', ' ); ?></h4>
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail('gallery-thumb'); ?>
		<div class="overlay">
			<div class="overlay-content">
				<h3><?php the_title(); ?></h3>
			</div>
		</div>
	</a>
</li>
