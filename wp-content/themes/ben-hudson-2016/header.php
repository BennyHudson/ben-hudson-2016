<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<div id="fb-root"></div>
</head>
<body <?php body_class('tastic'); ?>>

	<section class="page-content">
		<section class="footer-fix">

			<header>
				<?php get_template_part('snippets/nav', 'mobile'); ?>
				<section class="container">
					<aside class="header-main">
						<?php get_template_part('snippets/content', 'logo'); ?>
					</aside>
					<aside class="header-nav">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => '' ) ); ?>
						</nav>
					</aside>
				</section>
			</header>
