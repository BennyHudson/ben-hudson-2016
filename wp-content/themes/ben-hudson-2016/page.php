<?php get_header(); ?>

	<section class="container main narrow">
		<article class="page">
			<h1 class="page-title alt"><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</article>
	</section>

<?php get_footer(); ?>
