<li>
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail('gallery-thumb'); ?>
		<h2><?php the_title(); ?></h2>
	</a>
</li>
