<?php get_header(); ?>

	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'letterbox' ); ?>

	<section class="container main">
		<article>
			<div class="blog-feature" style="background-image: url('<?php echo $image[0]; ?>');">
				<div>
					<div>
						<h1 class="blog-title"><?php the_title(); ?></h1>
						<div class="post-meta">
							<span><i class="fa fa-calendar"></i> <?php the_time('jS F Y'); ?></span>
							<span><i class="fa fa-folder-open"></i> <?php the_category( ', ' ); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="article-body">
				<?php the_content(); ?>
			</div>
		</article>
	</section>

<?php get_footer(); ?>
